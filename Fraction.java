//package edu.slu.prog2;

/**
 * Name: ALCID, Karina Ysabelle L.
 * Class Code & Schedule: CS 122L 9301B 11 - 12:30 MTh
 * Start Date: February 28, 2019
 * Instructor: Dale D. Miguel
 */

public class Fraction {
    //data members'
    private int numerator;
    private int denominator;

    // default constructor
    public Fraction() {
        numerator = 0;
        denominator = 1;
    }

    // default constructor
    public Fraction(int wholeNumVal){
        this.numerator = wholeNumVal;
        denominator = 1;
    }

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    //accessor method for the numerator part of the fraction
    public int getNumerator() {
        return numerator;
    }

    //accessor method for the denominator part of the fraction
    public int getDenominator() {
        return denominator;
    }

    //setter method for the denominator part of the fraction
    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    //setter method for the numerator part of the fraction
    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    // Returns a string representation of the fraction
    public String toString(){
        String r = "";
        if (numerator == 0) {
            r = "0";
        } else
            if(denominator == 1)
                r = numerator + "";
            else
                r = numerator + "/" + denominator;
        return r;
    }

    public double toDouble(){
        return (double) numerator / denominator;
    }

    // Returns the reduced form of a given fraction
    public Fraction reduce(){
        Fraction r= new Fraction(); // construct a fraction
        int gCF = computeGCF(); // determine the greatest common factor of numerator and denominator
        //compute newN, the numerator of the simpliest form of this fraction
        int newN= numerator/gCF;
        //compute newD, the denominator of the simpliest form of this fraction
        int newD = denominator/gCF;
        r.setNumerator(newN); // set the numerator of the simpliest form to newN
        r.setDenominator(newD);// set the denominator of the simpliest form to newD
        return r; // return the simpliest form of this fraction
    }
    // Computes the greatest common factor of the numerator and denominator
    private int computeGCF() {
        int gCF = 1;
        int lesser = 1;
        boolean found = false;
        try {
            lesser = computeLesser(numerator, denominator);
            for (int candidate = lesser; !found; candidate--) {
                if (numerator % candidate == 0 && denominator % candidate == 0) {
                    found = true;
                    gCF = candidate;
                }
            }
        } catch (NullPointerException e1) {

        }
        return gCF;
    }

    //Returns the lesser integer between n1 and n2
    private int computeLesser(int n1, int n2){
        int lesser = n1;
        if (n1 < n2) {
            lesser = n1;
        } else {
            lesser = n2;
        }
        return lesser;
    }

    // Returns the sum of this fraction and another fraction
    public Fraction add(Fraction another){
        Fraction s = new Fraction();
        int den = denominator * another.getDenominator();
        int num = numerator * another.getDenominator() + denominator * another.getNumerator();
        s.setNumerator(num);
        s.setDenominator(den);
        return s.reduce();
    }

    // Returns the sum of this fraction and a mixed fraction
    public Fraction add(MixedFraction another){
        Fraction s = new Fraction();
        Fraction f2 = another.toFraction();
        int den = denominator * f2.getDenominator();
        int num = numerator * f2.getDenominator() + denominator * f2.getNumerator();
        s.setNumerator(num);
        s.setDenominator(den);
        return s.reduce();
    }

    // Returns the result of subtracting another fraction from this fraction
    public Fraction subtract(Fraction another){
        Fraction result = new Fraction();
        int den = denominator * another.getDenominator();
        int num = numerator * another.getDenominator() - denominator * another.getNumerator();
        result.setNumerator(num);
        result.setDenominator(den);
        return result.reduce();
    }

    // Returns the result of subtracting a mixed fraction from this fraction
    public Fraction subtract(MixedFraction another){
        Fraction result = new Fraction();
        Fraction f2 = another.toFraction();
        int den = denominator * f2.getDenominator();
        int num = numerator * f2.getDenominator() - denominator * f2.getNumerator();
        result.setNumerator(num);
        result.setDenominator(den);
        return result.reduce();
    }

    // Returns the result of multiplying this fraction by another fraction
    public Fraction multiplyBy(Fraction another) {
        Fraction result = new Fraction();
        int den = denominator * another.getDenominator();
        int num = numerator * another.getNumerator();
        result.setNumerator(num);
        result.setDenominator(den);
        return result.reduce();
    }

    // Returns the result of multiplying this fraction by a mixed fraction
    public Fraction multiplyBy(MixedFraction another){
        Fraction result = new Fraction();
        Fraction f2 = another.toFraction();
        int num = numerator * f2.getNumerator();
        int den = denominator * f2.getDenominator();
        result.setNumerator(num);
        result.setDenominator(den);
        return result.reduce();
    }

     // Returns the result of dividing this fraction by another fraction
    public Fraction divideBy(Fraction another){
        Fraction result = new Fraction();
        int den = denominator * another.getNumerator();
        int num = numerator * another.getDenominator();
        result.setNumerator(num);
        result.setDenominator(den);
        return result.reduce();
    }

     //Returns the result of dividing this fraction by a mixed fraction
    public Fraction divideBy(MixedFraction another){
        Fraction result = new Fraction();
        Fraction f2 = another.toFraction();
        System.out.println(f2.toString());
        int den = denominator * f2.getNumerator();
        System.out.println(den);
        int num = numerator * f2.getDenominator();
        System.out.println(num);
        result.setNumerator(num);
        result.setDenominator(den);
        return result.reduce();
    }


    /**
     *Returns a mixed fraction equivalent to this fraction
     **/
    public MixedFraction toMixedFraction(){
        int w = numerator / denominator; // whole number part
        int numOfFractionPart = numerator % denominator;
        int denOfFractionPart = denominator;
        Fraction fPart = new Fraction(numOfFractionPart, denOfFractionPart);
        MixedFraction result = new MixedFraction(numOfFractionPart, denOfFractionPart, w);
        return result;
    }
} // end of Fraction class
