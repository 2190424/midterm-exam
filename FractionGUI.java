//package edu.slu.prog2;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Name: ALCID, Karina Ysabelle L.
 * Class Code & Schedule: CS 122L 9301B 11 - 12:30 MTh
 * Start Date: March 16. 2019
 * Instructor: Dale D. Miguel
 */

public class FractionGUI {
    // data members
    private JFrame frame = new JFrame("Fraction Calculator");
    private JLabel fraction1Label = new JLabel("Fraction 1: ");
    private JLabel fraction2Label = new JLabel("Fraction 2: ");
    private JLabel resultLabel = new JLabel("Result: ");
    private JTextField fraction1TextField = new JTextField();
    private JTextField fraction2TextField = new JTextField();
    private JTextField resultTextField = new JTextField();
    private JButton ADDITION = new JButton("+");
    private JButton SUBTRACTION = new JButton("-");
    private JButton MULTIPLICATION = new JButton("*");
    private JButton DIVISION = new JButton("/");
    private Fraction firstFraction;
    private MixedFraction secondFraction;
    private MixedFraction resultingFraction;

    // Default constructor
    public FractionGUI(){
        //Sets the locations of the given components
        fraction1Label.setBounds(20, 50,95,40);
        fraction1Label.setHorizontalAlignment(0);
        fraction1TextField.setBounds(100,50,200,40);

        fraction2Label.setBounds(20, 100,95,40);
        fraction2Label.setHorizontalAlignment(0);
        fraction2TextField.setBounds(100, 100, 200, 40);

        resultLabel.setBounds(20, 200, 95, 40);
        resultLabel.setHorizontalAlignment(0);
        resultTextField.setBounds(100,200,200,40);
        resultTextField.setEditable(false);

        // Addition button ActionListener to allow the addition of the given inputs from the user
        ADDITION.setBounds(30, 150, 60, 35);
        ADDITION.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                firstFraction = initializeFraction1(fraction1TextField.getText());
                secondFraction = initializeFraction2(fraction2TextField.getText());
                resultingFraction = (firstFraction.add(secondFraction)).toMixedFraction();
                resultTextField.setText(resultingFraction.toString());
            }
        });

        // Subtraction button ActionListener to allow the subtraction of the given inputs from the user
        SUBTRACTION.setBounds(100, 150, 60, 35);
        SUBTRACTION.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                firstFraction = initializeFraction1(fraction1TextField.getText());
                secondFraction = initializeFraction2(fraction2TextField.getText());
                resultingFraction = (firstFraction.subtract(secondFraction)).toMixedFraction();
                resultTextField.setText(resultingFraction.toString());
            }
        });

        // Multiplication button ActionListener to allow the multiplication of the given inputs from the user
        MULTIPLICATION.setBounds(170, 150, 60, 35);
        MULTIPLICATION.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                firstFraction = initializeFraction1(fraction1TextField.getText());
                secondFraction = initializeFraction2(fraction2TextField.getText());
                resultingFraction = (firstFraction.multiplyBy(secondFraction)).toMixedFraction();
                resultTextField.setText(resultingFraction.toString());
            }
        });

        // Division button ActionListener to allow the division of the given inputs from the user
        DIVISION.setBounds(240, 150, 60, 35);
        DIVISION.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                firstFraction = initializeFraction1(fraction1TextField.getText());
                secondFraction = initializeFraction2(fraction2TextField.getText());
                resultingFraction = ((firstFraction.divideBy(secondFraction)).toMixedFraction());
                resultTextField.setText(resultingFraction.toString());
            }
        });

        //Adds the components to the JFrame
        frame.add(fraction1Label);
        frame.add(fraction2Label);
        frame.add(resultLabel);
        frame.add(fraction1TextField);
        frame.add(fraction2TextField);
        frame.add(resultTextField);
        frame.add(ADDITION);
        frame.add(SUBTRACTION);
        frame.add(MULTIPLICATION);
        frame.add(DIVISION);
        frame.setSize(350, 360);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    // main method of the FractionGUI class
    public static void main(String[] args) {
        new FractionGUI();
    }

    // Returns a Fraction based on a given String from a Fraction1TextField
    public static Fraction initializeFraction1(String fraction){
        JFrame errorFrame = new JFrame("Error Message");
        Fraction fraction1 = new Fraction();
        try {
            String[] f1Parts = (fraction.split("/"));
            int[] intArray = toIntArray(f1Parts);
            fraction1.setNumerator(intArray[0]);
            fraction1.setDenominator(intArray[1]);
        } catch (Exception x){
            JOptionPane.showMessageDialog(errorFrame, "Error, please enter a valid Fraction.", "Invalid Input for Fraction 1" ,JOptionPane.ERROR_MESSAGE);
        }
        return fraction1;
    }

    // Returns a Mixed Fraction based on a given String from a Fraction2TextField
    public static MixedFraction initializeFraction2(String fraction){
        JFrame errorFrame = new JFrame("Error Message");
        MixedFraction fraction2 = new MixedFraction();
        try {
            String[] f1Parts = (fraction.split("[ /]"));
            int[] intArray = toIntArray(f1Parts);
            fraction2.setWholePart(intArray[0]);
            fraction2.setNumerator(intArray[1]);
            fraction2.setDenominator(intArray[2]);
        } catch (Exception x){
            JOptionPane.showMessageDialog(errorFrame, "Error, please enter a valid Mixed Fraction.", "Invalid Input for Fraction 2" ,JOptionPane.ERROR_MESSAGE);
        }
        return fraction2;
    }

    // Changes the String array into an int array by parsing the values at the indicated index
    public static int[] toIntArray(String[] initial){
        int[] hold = new int[initial.length];
        for(int x = 0; x < initial.length; x++){
            hold[x] = Integer.parseInt(initial[x]);
        }
        if (hold[hold.length - 1] < 0){ //checks if the denominator is a negative number
            hold[hold.length - 1] *= -1; //this ensures that the denominator is never negative
            hold[hold.length - 2] *= -1; //this makes the numerator a negative number instead
        }
        return hold;
    }
}