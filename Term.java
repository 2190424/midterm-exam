//package edu.slu.prog2.fingrpact;

/**
 * A Java class that will be used to hold (paired) data:
 * the numerical coefficient in either Integer or Double
 * type and the exponent in Integer
 * <p>
 * The Java class includes two constructors because
 * of the implementation of a Divide operation in the
 * Polynomial class. Besides that it also includes a
 * method to override the default toString method wherein
 * it will print a formatted String of the Term object
 * </p>
 * <p>v.1.0. Default constructor and setter/getter methods</p>
 * <p>v.1.1. Implemented a new constructor and toString method</p>
 * <p>v.1.2. Changed public methods to package-private methods</p>
 * 9301L CS122L 11:00AM-12:30PM MTH
 * @author BANDA-AY, Kaezer : MELEGRITO, Aaron : TOMELDEN, Ryan : VENTURINA, Rovin
 * @version 1.2.
 * @since 2019-05-12
 */

class Term {
    private int coefficient; //holds the coefficient in Integer type
    private double divisionCoefficient; //holds the coefficient in Double type
    private int exponent; //holds the exponent in Integer type

    /**
     * Constructor for a Term object, contains the numerical
     * coefficient and a non-negative integer exponent
     * @param coefficient The coefficient of the Term object
     * @param exponent The exponent of the Term object
     */
    Term(int coefficient, int exponent){
        this.coefficient = coefficient;
        this.exponent = exponent;
    }

    /**
     * Bonus Requirement // Optional
     * Constructor for a Term object used for Polynomial division,
     * contains the numerical coefficient in primitive double type
     * and a non-negative integer exponent
     * @param coefficient The coefficient of the Term object in primitive double type
     * @param exponent The exponent of the Term object
     */
    Term(double coefficient, int exponent){
        this.divisionCoefficient = coefficient;
        this.exponent = exponent;
    }

    /**
     * Getter method for the coefficient of the Term object
     * @return The coefficient of the Term object
     */
    int getCoefficient() {
        return coefficient;
    }

    /**
     * Getter method for the coefficient in Double type of the Term object
     * @return The coefficient in Double type of the Term object
     */
    double getDivisionCoefficient() {
        return divisionCoefficient;
    }

    /**
     * Getter method for the exponent of the Term object
     * @return The exponent of the Term object
     */
    int getExponent() {
        return exponent;
    }

    /**
     * Setter method for the coefficient of the Term object
     * ***Optional, but just in case. Remove if no use is found and set coefficient to final.
     * @param coefficient Sets the coefficient of the Term object
     */
    private void setCoefficient(int coefficient) {
        this.coefficient = coefficient;
    }

    /**
     * Setter method for the exponent of the Term object
     * ***Optional, but just in case. Remove if no use is found and set exponent to final.
     * @param exponent Sets the exponent of the Term object
     */
    private void setExponent(int exponent) {
        this.exponent = exponent;
    }

    /**
     * Returns a String format of the Term object
     * @param variable Sets the variable (letter) to be used in the printing the Term
     * @return A String containing the Term, its coefficient, and the exponent
     */
    public String toString(char variable){
        String stringTermFormat = "Term: "+getCoefficient()+variable+"^"+getExponent();
        return String.format("%-20s%s%-14d%s%d%n",stringTermFormat,"Coefficient: ",getCoefficient(),"Exponent: ",getExponent());
    }
    public String toStringDouble(char variable){
        String stringTermFormat = "Term: "+getDivisionCoefficient()+variable+"^"+getExponent();
        return String.format("%-20s%s%-14f%s%d%n",stringTermFormat,"Coefficient: ",getDivisionCoefficient(),"Exponent: ",getExponent());
    }
}
