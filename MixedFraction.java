//package edu.slu.prog2;

/**
 * Name: ALCID, Karina Ysabelle L.
 * Class Code & Schedule: CS 122L 9301B 11 - 12:30 MTh
 * Start Date: February 28, 2019
 * Instructor: Dale D. Miguel
 */

public class MixedFraction extends Fraction {
    private int wholePart;

    /**
     * This default constructor sets the whole number part of a mixed fraction to 0
     * the numerator of the fraction part to 0 and the denominator of the
     * fraction part to 1
     */
    public MixedFraction() {
        super();
        wholePart = 0;
    }

    /**
     * Constructs a mixed fraction with a given whole number part and numerator
     * of fraction part and denominator of fraction part.
     */
    public MixedFraction(int numerator, int denominator, int wholePart) {
        super(numerator, denominator);
        this.wholePart = wholePart;
    }

    /**
     * Sets the whole number part of a mixed fraction to a given whole number
     */
    public void setWholePart(int wholePart) {
        this.wholePart = wholePart;
    }

    /**
     * Returns the whole part of a mixed number
     */
    public int getWholePart() {
        return wholePart;
    }

    /**
     * Returns a string represent a mixed fraction.
     * 2 and 1/3 is an example of a mixed fraction.
     */
    public String toString(){
        String r = "";
        if(wholePart == 0)
            r = super.toString();
        else if(super.toDouble() == 0)
            r = wholePart + "";
        else
            r = (wholePart + " " + super.toString());
        return r;
    }

    /**
     * Returns an improper fraction equivalent to a mixed number
     */
    public Fraction toFraction() {
        int num;
        if(wholePart < 0){
            num = getDenominator() * wholePart - getNumerator();
        } else
            num = getDenominator() * wholePart + getNumerator();
        int den = getDenominator();
        return (new Fraction(num, den));
    }
    /**
     * Returns the sum of this mixed fraction and another mixed fraction.
     * Algorithm:
     * a. Convert this mixed fraction to an equivalent improper fraction
     * b. Convert the other mixed fraction to an equivalent improper fraction
     * c. add the two resulting fractions from steps a and b
     * d. Convert the result of step c to a mixed fraction form
     * e. return the result of step d
     */
    public MixedFraction add(MixedFraction other){
        Fraction first= this.toFraction();
        Fraction second = other.toFraction();
        Fraction sum = first.add(second);
        MixedFraction result = sum.toMixedFraction();
        return result;
    }
    /** Returns the difference of this mixed fraction and another mixed fraction.
     * Algorithm:
     * a. Convert this mixed fraction to an equivalent improper fraction
     * b. Convert the other mixed fraction to an equivalent improper fraction
     * c. subtract the result of step b from the result of step a
     * d. Convert the result of step c to a mixed fraction form
     * e. return the result of step d
     */
    public MixedFraction subtract(MixedFraction other){
        Fraction first= this.toFraction();
        Fraction second = other.toFraction();
        Fraction dif = first.subtract(second);
        MixedFraction result = dif.toMixedFraction();
        return result;
    }
    /** Returns the product of this mixed fraction and another mixed fraction.
    * Algorithm:
    * a. Convert this mixed fraction to an equivalent improper fraction
    * b. Convert the other mixed fraction to an equivalent improper fraction
    * c. multiply the result of step a by the result of step b
    * d. Convert the result of step c to a mixed fraction form
    * e. return the result of step d
    */
    public MixedFraction multiplyBy(MixedFraction other) {
        Fraction first = this.toFraction();
        Fraction second = other.toFraction();
        Fraction p = first.multiplyBy(second);
        MixedFraction result = p.toMixedFraction();
        return result;
    }
    /**Returns the quotient of this mixed fraction and another mixed fraction.
     *Algorithm:
     *a. Convert this mixed fraction to an equivalent improper fraction
     *b. Convert the other mixed fraction to an equivalent improper fraction
     *c. divde the result of step a by the result of step b
     *d. Convert the result of step c to a mixed fraction form
     *e. return the result of step d
     **/
    public MixedFraction divideBy(MixedFraction other){
        Fraction first= this.toFraction();
        Fraction second = other.toFraction();
        Fraction q = first.divideBy(second);
        MixedFraction result = q.toMixedFraction();
        return result;
    }
    /**
     * Returns true of this fraction has the same
     * decimal value as another mixed number
     */
    public boolean equals(MixedFraction another){
        boolean result = false;
        result = (toDecimal() == another.toDecimal());
        return result;
    }
    /**
     * Returns the decimal value of this mixed number
     */
    public double toDecimal(){
        return (wholePart + 1.0 * getNumerator() / getDenominator());
    }
}
