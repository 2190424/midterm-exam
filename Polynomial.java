//package edu.slu.prog2.fingrpact;

import sun.reflect.generics.tree.Tree;

import java.util.*;
import java.lang.Math;

/**
 * A Java class that will be used to hold (paired) data:
 * the variable of a polynomial object in Character data
 * type and a list of Term objects in a List data structure
 * <p>
 * The Java class has two separate methods to deconstruct a
 * Polynomial String (toStringTermArray and toTermList). The
 * class also includes a toString method that makes use of
 * two other methods (reduce and outputPolynomial) to print
 * out a String that is already ordered by exponent.
 * </p>
 * <p>v.1.0. Default constructor and setter/getter methods</p>
 * <p>v.1.1. Implemented a method to deconstruct a String into a Polynomial object</p>
 * <p>v.1.2. Implemented the evaluate, add, subtract, and multiply methods</p>
 * <p>v.1.3. Implemented a new toString method</p>
 * 9301L CS122L 11:00AM-12:30PM MTH
 * @author BANDA-AY, Kaezer : MELEGRITO, Aaron : TOMELDEN, Ryan : VENTURINA, Rovin
 * @version 1.3.
 * @since 2019-05-12
 */

public class Polynomial {
    private char variable; //holds the variable in Character type
    private List<Term> terms; //holds the list of Term objects in a List data structure

    /**
     * Constructor for a Polynomial object, contains the
     * variable used in Character type and a list of Term
     * objects in a List data structure
     * @param variable The variable used in the Polynomial expression
     * @param terms The list of Term objects found in the Polynomial expression
     */
    public Polynomial(char variable, List<Term> terms){
        this.variable = variable;
        this.terms = terms;
    }

    /**
     * Bonus Requirement // Optional
     * An empty Constructor for a Polynomial object used for Polynomial Division
     */
    public Polynomial(){
        List<Term> empty = new ArrayList<>();
        this.terms = empty;
    }

    /**
     * Getter method for the variable of the Polynomial object
     * @return The variable of the Polynomial object
     */
    private char getVariable() {
        return variable;
    }

    /**
     * Getter method for the list of terms of the Polynomial object
     * @return The list of terms of the Polynomial object
     */
    private List<Term> getTerms() {
        return terms;
    }

    /**
     * Return a String array containing all the terms in a polynomial String
     * @param polynomial Polynomial String to parse
     * @return A String array of the given polynomial String
     */
    public static String[] toStringTermArray(String polynomial){
        //Remove all occurrences of spaces in the String
        //Replace all negative coefficients into the form +- i.e. -2x turns to +-2x
        //Split terms at a plus sign i.e. 4x^2-5x-7 -> 4x^2+-5x-7 -> [4x^2, -5x, -7]
        String[] stringTermArray;
        if(polynomial.matches(".*[\\^-]")){
            System.out.println("The polynomial contains negative exponents.");
            System.exit(0);
            return stringTermArray = new String[0];
        } else {
            String removeSpaces = polynomial.replaceAll(" ", "");
            stringTermArray = removeSpaces.replaceAll("-", "+-").split("\\+");
        }
        return stringTermArray;
    }

    /**
     * Method that creates Term objects from a String Array
     * @param stringTermArray String Array that contains separated polynomial terms
     * @return List data structure containing Term objects
     */
    public static List<Term> toTermList(String[] stringTermArray){
        //Holds the variables
        List<Term> inputTerms = new ArrayList<>();
        int coefficient = 0, exponent = 0;
        //Parsing algorithm
        for(String singleTerm: stringTermArray){
            String[] getCoefficients = singleTerm.split("\\^\\d+\\+?");
            for(String newTerm: getCoefficients){
                if(newTerm.isEmpty()){
                    continue;
                } else if(newTerm.equalsIgnoreCase("x")){
                    coefficient = 1;
                    //System.out.println("1");
                } else if(newTerm.matches("-?\\d?\\w")){
                    String[] splitVar = newTerm.split("x");
                    if(splitVar[0].equalsIgnoreCase("-")){
                        coefficient = -1;
                        //System.out.println(-1);
                    } else {
                        coefficient = Integer.parseInt(splitVar[0]);
                        //System.out.println(splitVar[0]);
                    }
                } else {
                    String[] splitVar = newTerm.split("x");
                    coefficient = Integer.parseInt(splitVar[0]);
                    //System.out.println(splitVar[0]);
                }
            }
            String[] getExponents = singleTerm.split("\\^");
            if(getExponents[0].isEmpty()) {
                exponent = 0;
            } else if(getExponents.length > 1){
                if(Integer.parseInt(getExponents[1])<0){
                    System.out.println("The exponent is negative.");
                    break;
                } else {
                    exponent = Integer.parseInt(getExponents[1]);
                    //System.out.println(getExponents[1]);
                }
            } else if(getExponents[0].matches("-?\\d?[a-zA-Z]+")){
                exponent = 1;
            } else if(getExponents[0].matches(".*[a-zA-Z].*")){
                exponent = 1;
            } else {
                exponent = 0;
            }
            Term properTerm = new Term(coefficient,exponent);
            inputTerms.add(properTerm);
        }
        return inputTerms;
    }

    /**
     * Returns the degree of a Polynomial object
     * @return The degree of the Polynomial object
     */
    public int getDegree(){
            int[] exponents = getExponents();
            Arrays.sort(exponents);
            return exponents[exponents.length - 1];
    }

    /**
     * Method to get the coefficients of a Polynomial object
     * @return Integer array of coefficient/s
     */
    public int[] getCoefficients(){
        int[] coefficients = new int[terms.size()];
        for(int index = 0; terms.size() > index; index++){
            coefficients[index] = terms.get(index).getCoefficient();
        }
        return coefficients;
    }

    /**
     * Method to get the exponents of a Polynomial object
     * @return Integer array of exponents/s
     */
    public int[] getExponents(){
        int[] exponents = new int[terms.size()];
        for(int index = 0; terms.size() > index; index++){
            exponents[index] = terms.get(index).getExponent();
        }
        return exponents;
    }

    /**
     * Method that will evaluate a Polynomial object based on the given value for the variable
     * @param valueOfVariable The value which will be used substitute the variable in the Polynomial expression
     * @return Integer value of the evaluated Polynomial
     */
    public double evaluate(double valueOfVariable){
        double evaluatedValue = 0.0;
        TreeMap<Integer,Integer> evaluatePolynomial = reduce(getCoefficients(),getExponents());
        Set<Integer> availableExponents = evaluatePolynomial.keySet();
        for(int exponent: availableExponents){
            evaluatedValue += Math.pow(valueOfVariable, exponent) * evaluatePolynomial.get(exponent);
        }
        return evaluatedValue;
    }

    /**
     * Method that will simplify a Polynomial object, adding or subtracting similar terms
     * @param coefficients Integer Array of coefficients of the Polynomial object
     * @param exponents Integer Array of exponents of the Polynomial object
     * @return Numerically ordered TreeMap of Coefficients (Value) and Exponents (Key)
     */
    public TreeMap<Integer,Integer> reduce(int[] coefficients, int[] exponents){
        TreeMap<Integer,Integer> reducePolynomial = new TreeMap<>();
        for(int index = 0; index < coefficients.length; index++){
            int mapCoefficient = coefficients[index];
            int mapExponent = exponents[index];
            if(reducePolynomial.containsKey(mapExponent)){
                int currentCoefficient = reducePolynomial.get(mapExponent);
                reducePolynomial.put(mapExponent, currentCoefficient + mapCoefficient);
            } else {
                reducePolynomial.put(mapExponent, mapCoefficient);
            }
        }
        return reducePolynomial;
    }

    /**
     * Extended method of the toString method. Returns a reduced Polynomial object
     * @param polynomials The TreeMap of the Polynomial object which contains the coefficients and exponents
     * @return The String format of the reduced Polynomial object
     */
    public String outputPolynomial(TreeMap<Integer,Integer> polynomials) {
        StringBuilder builder = new StringBuilder();
        int counter = 0;
        for(Map.Entry<Integer,Integer> polynomial : polynomials.descendingMap().entrySet())
        {
            if(polynomial.getValue() != 0)
            {//Value is coefficient key is exponent
                // append plus (note negative coefficient will print out a minus sign)
                if(polynomial.getValue() >= 0)
                {
                    if(counter!=0)
                    {
                        builder.append("+ ");
                    }
                    counter++;
                    builder.append(polynomial.getValue());
                }
                if(polynomial.getValue() < 0)
                {
                    if(polynomial.getKey()==0) {
                        String term = Integer.toString(polynomial.getValue());
                        char[] parts = term.toCharArray();
                        builder.append(" - ");
                        for (int x = 1; x < parts.length; x++)
                            builder.append(parts[x]);
                    } else {
                        String term = Integer.toString(polynomial.getValue());
                        char[] parts = term.toCharArray();
                        builder.append("- ");
                        for (int x = 1; x < parts.length; x++)
                            builder.append(parts[x]);
                    }
                }
                // append coefficient
                if(polynomial.getKey()==0)
                    break;
                else
                {
                    builder.append(getVariable());
                    if(polynomial.getKey()==1) {
                        continue;
                    }else {
                        builder.append("^");
                        builder.append(polynomial.getKey());
                    }
                }

                //builder.append(polynomial.getKey());
                builder.append(" ");
            }
        }
        //if(polynomials.containsKey(0)&&polynomials.get(polynomials.firstKey())!=0) {
        //    builder.append(" + " + polynomials.get(0));
        //}
        return builder.toString();
    }

    /**
     * Makes use of two separate methods to turn a Polynomial object into a String
     * @return A formatted String of a Polynomial object
     */
    public String toString(){
        return outputPolynomial(reduce(getCoefficients(),getExponents()));
    }

    /**
     * Method that will add two Polynomial objects
     * @param another The polynomial that will be added to
     * @return The sum of the two Polynomials
     */
    public Polynomial add(Polynomial another){
        TreeMap<Integer,Integer> poly1 = reduce(getCoefficients(),getExponents());
        TreeMap<Integer,Integer> poly2 = reduce(another.getCoefficients(),another.getExponents());
        TreeMap<Integer,Integer> sum = new TreeMap<>();

        for(Map.Entry<Integer,Integer> augend: poly1.entrySet()){
            int exponent = augend.getKey();
            int coefficient = augend.getValue();
            int coefficientFragment;
            if(poly2.containsKey(exponent)){
                coefficientFragment = coefficient + poly2.get(exponent) ;
            } else {
                coefficientFragment = coefficient;
            }
            sum.put(exponent,coefficientFragment);
        }
        for(Map.Entry<Integer,Integer> addend: poly2.entrySet()){
            if(sum.containsKey(addend.getKey())){
                continue;
            } else {
                sum.put(addend.getKey(),addend.getValue());
            }
        }

        List<Term> sumTerms = new ArrayList<>();
        for(Map.Entry<Integer,Integer> term: sum.entrySet()){
            Term sumTermFragment = new Term(term.getValue(),term.getKey());
            sumTerms.add(sumTermFragment);
        }
        return new Polynomial(getVariable(),sumTerms);
    }

    /**
     * Method that will subtract two Polynomial objects
     * @param another The polynomial that will be subtracted to
     * @return The difference of the two Polynomials
     */
    public Polynomial subtract(Polynomial another){
        TreeMap<Integer,Integer> poly1 = reduce(getCoefficients(),getExponents());
        TreeMap<Integer,Integer> poly2 = reduce(another.getCoefficients(),another.getExponents());
        TreeMap<Integer,Integer> difference = new TreeMap<>();

        for(Map.Entry<Integer,Integer> minuend: poly1.entrySet()){
            int exponent = minuend.getKey();
            int coefficient = minuend.getValue();
            int coefficientFragment;
            if(poly2.containsKey(exponent)){
                coefficientFragment =  coefficient - poly2.get(exponent);
            } else {
                coefficientFragment = coefficient;
            }
            difference.put(exponent,coefficientFragment);
        }
        for(Map.Entry<Integer,Integer> subtrahend: poly2.entrySet()){
            if(difference.containsKey(subtrahend.getKey())){
                continue;
            } else {
                difference.put(subtrahend.getKey(),-subtrahend.getValue());
            }
        }

        List<Term> differenceTerms = new ArrayList<>();
        for(Map.Entry<Integer,Integer> term: difference.entrySet()){
            Term sumTermFragment = new Term(term.getValue(),term.getKey());
            differenceTerms.add(sumTermFragment);
        }
        return new Polynomial(getVariable(),differenceTerms);
    }

    /**
     * Method that will multiply two Polynomial objects
     * @param another The polynomial that will be multiplied to
     * @return The product of the two Polynomials
     */
    public Polynomial multiply(Polynomial another){
        TreeMap<Integer,Integer> poly1 = reduce(getCoefficients(),getExponents());
        TreeMap<Integer,Integer> poly2 = reduce(another.getCoefficients(),another.getExponents());
        TreeMap<Integer,Integer> product = new TreeMap<>();

        for(Map.Entry<Integer, Integer> multiplicand: poly1.entrySet())
        {
            int coefficient = multiplicand.getValue();
            int exponent = multiplicand.getKey();
            int coefficientFragment;
            int exponentFragment;
            for(Map.Entry<Integer, Integer> multiplier: poly2.entrySet()) {
                coefficientFragment = coefficient*multiplier.getValue();
                exponentFragment = exponent+multiplier.getKey();
                if(product.containsKey(exponentFragment)){
                    int existing = coefficientFragment + product.get(exponentFragment);
                    product.put(exponentFragment,existing);
                } else {
                    product.put(exponentFragment,coefficientFragment);
                }
            }
        }

        List<Term> productTerms = new ArrayList<>();
        for(Map.Entry<Integer,Integer> term: product.entrySet()){
            Term multiplyTermFragment = new Term(term.getValue(),term.getKey());
            productTerms.add(multiplyTermFragment);
        }
        return new Polynomial(getVariable(),productTerms);
    }

    /**
     * Method that turns a TreeMap data structure into a Polynomial object
     * @param tMap TreeMap to be converted into a Polynomial object
     * @return A Polynomial object based on the given TreeMap data structure
     */
    public Polynomial toPolynomial(TreeMap<Integer,Double> tMap){
        List<Term> converterList = new ArrayList<>();
        for(Map.Entry<Integer,Double> converterMap : tMap.entrySet()){
            Term convertTerm = new Term(converterMap.getValue(),converterMap.getKey());
            converterList.add(convertTerm);
        }
        Polynomial convertedPoly = new Polynomial(getVariable(),converterList);
        return convertedPoly;
    }

    /**
     * Method that turns a Polynomial object into a TreeMap data structure
     * @param tPoly Polynomial object to be converted into a TreeMap
     * @return A TreeMap data structure based on the given Polynomial object
     */
    public TreeMap<Integer,Double> toTreeMap(Polynomial tPoly){
        List<Term> converterList = tPoly.getTerms();
        TreeMap<Integer,Double> converterMap = new TreeMap<>();
        for(Term convertTerm : converterList){
            converterMap.put(convertTerm.getExponent(),convertTerm.getDivisionCoefficient());
        }
        return converterMap;
    }

    public  Polynomial divide(Polynomial another){
        TreeMap<Integer,Integer> num = reduce(getCoefficients(),getExponents());
        TreeMap<Integer,Integer> den = reduce(another.getCoefficients(),another.getExponents());
        TreeMap<Integer,Double> quotient = new TreeMap<>();
        TreeMap<Integer,Double> dNum = new TreeMap<>();
        TreeMap<Integer,Double> dDen = new TreeMap<>();
        for(Map.Entry<Integer,Integer> copy1 : num.entrySet()){
            dNum.put(copy1.getKey(),(double)copy1.getValue());
        }
        for(Map.Entry<Integer,Integer> copy2 : den.entrySet()){
            dDen.put(copy2.getKey(),(double)copy2.getValue());
        }
        List<Term> quotientTerms = new ArrayList<>();
        List<Term> quotientMultiplier = new ArrayList<>();
        int counter = 0;
        while(dNum.get(dNum.lastKey())>=dDen.get(dDen.lastKey())){
            double dCo = dNum.get(dNum.lastKey()) / dDen.get(dDen.lastKey());
            int dEx = dNum.lastKey() - dDen.lastKey();
            System.out.println("coefficient: "+dCo+" ||| exponent: "+dEx); //testing
            Term dTerm = new Term(dCo,dEx); //new term from dividing den with num
            if(counter==0) {
                quotientMultiplier.add(dTerm); //term that will multiply with denominator
                counter++;
            }
            quotientTerms.add(dTerm); //list of the quotient terms
            quotient.put(dEx,dCo);
            Polynomial multiplied = new Polynomial(getVariable(),quotientMultiplier);
            Polynomial toBeSubtractedWith = multiplied.multiply(toPolynomial(dDen));
            Polynomial newPolyAfterSub = toPolynomial(dNum).subtract(toBeSubtractedWith);
            dNum = toTreeMap(newPolyAfterSub);
            System.out.println(counter + " " + dNum.entrySet()+"\n======");
        }

        System.out.println(quotient.entrySet());
        for(Map.Entry<Integer,Double> term: quotient.entrySet()){
            String sQuotient = term.getValue()+""+getVariable()+"^"+term.getKey();
            System.out.println(sQuotient);
            Term divideTermFragment = new Term(term.getValue(),term.getKey());
            quotientTerms.add(divideTermFragment);
        }
        return new Polynomial(getVariable(),quotientTerms);
    }

    //Bonus Requirement
    //public Polynomial divide(Polynomial another){};
}
