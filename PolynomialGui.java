package edu.slu.prog2.fingrpact;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;
import java.util.Date;

/**
 * A Java Program that makes use of the Polynomial and Term class to
 * manipulate Polynomial Strings
 * 9301L CS122L 11:00AM-12:30PM MTH
 * @author BANDA-AY, Kaezer : MELEGRITO, Aaron : TOMELDEN, Ryan : VENTURINA, Rovin
 * @version 1.0
 * @since 2019-05-12
 */
public class PolynomialGui implements ActionListener{
    JTextField poly1TF, poly2TF,varTF,resultTF;
    JButton addButton, subButton, mulButton, divButton, clearButton, evalPoly1Button, evalPoly2Button, printButton;
    final char VAR = 'x';

    public static void main(String[] args) throws IOException{
        new PolynomialGui();
    }

    public PolynomialGui() throws IOException{
        guiPoly();
    }

    public void guiPoly()throws IOException{
        JFrame frame = new JFrame();
        frame.setSize(420,550);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setLayout(null);


        JLabel title = new JLabel("POLYNOMIAL CALCULATOR");
        title.setBounds(88,2,240,35);
        title.setFont(new Font("Courier New",Font.BOLD,18));

        JLabel by = new JLabel("by:");
        by.setBounds(200,15,30,35);
        by.setFont(new Font("Courier New",Font.PLAIN,10));

        JLabel programmers = new JLabel("BANDAAY-MELEGRITO-TOMELDEN-VENTURINA");
        programmers.setBounds(77,30,320,35);
        programmers.setFont(new Font("Courier New",Font.PLAIN,11));

        JLabel preCaut = new JLabel("Use 'x' as your variable ONLY");
        preCaut.setBounds(100,45,320,35);
        preCaut.setFont(new Font("Courier New",Font.PLAIN,11));

        JLabel poly1 = new JLabel("Polynomial 1:");
        poly1.setBounds(45,70,90,40);

        JLabel poly2 = new JLabel("Polynomial 2:");
        poly2.setBounds(45,110,90,40);

        JLabel variable = new JLabel("Variable Value (x):");
        variable.setBounds(45,150,130,40);

        JLabel result = new JLabel("Result:");
        result.setBounds(45,385,60,40);

        evalPoly1Button = new JButton("Evaluate Polynomial 1");
        evalPoly1Button.setBounds(35,200,160,30);
        evalPoly1Button.addActionListener(this);
        evalPoly2Button = new JButton("Evaluate Polynomial 2");
        evalPoly2Button.setBounds(205,200,160,30);
        evalPoly2Button.addActionListener(this);

        addButton = new JButton("ADD");
        addButton.setBounds(170,240,65,30);
        addButton.addActionListener(this);

        subButton = new JButton("SUBTRACT");
        subButton.setBounds(145,275,110,30);
        subButton.addActionListener(this);

        mulButton = new JButton("MULTIPLY");
        mulButton.setBounds(145,310,110,30);
        mulButton.addActionListener(this);

        divButton = new JButton("DIVIDE");
        divButton.setBounds(145,345,110,30);
        divButton.addActionListener(this);

        clearButton = new JButton("CLEAR");
        clearButton.setBounds(160,430,85,30);
        clearButton.addActionListener(this);

        printButton = new JButton("PRINT TRANSACTION");
        printButton.setBounds(125,465,160,30);
        printButton.addActionListener(this);

        poly1TF = new JTextField();
        poly1TF.setBounds(130,75,210,30);
        poly2TF = new JTextField();
        poly2TF.setBounds(130,115,210,30);
        varTF = new JTextField();
        varTF.setBounds(160,155,80,30);
        resultTF = new JTextField();
        resultTF.setBounds(100,390,260,30);
        resultTF.setEditable(false);

        frame.add(title); frame.add(poly1); frame.add(poly2); frame.add(by); frame.add(programmers); frame.add(preCaut); //frame.add(divButton);
        frame.add(addButton);frame.add(subButton); frame.add(mulButton); frame.add(evalPoly1Button); frame.add(evalPoly2Button);
        frame.add(result);frame.add(clearButton);frame.add(poly1TF); frame.add(poly2TF); frame.add(variable);
        frame.add(varTF); frame.add(resultTF); frame.add(printButton);

        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e){
        String action = e.getActionCommand();
        String [] newTerm1 ,newTerm2;
        Polynomial a,b;
        switch (action){
            case "Evaluate Polynomial 1":
                evaluateTerm(1);
                break;
            case "Evaluate Polynomial 2":
                evaluateTerm(2);
                break;
            case "ADD":
                try {
                    newTerm1 = Polynomial.toStringTermArray(poly1TF.getText());
                    a = new Polynomial(VAR, Polynomial.toTermList(newTerm1));
                    newTerm2 = Polynomial.toStringTermArray(poly2TF.getText());
                    b = new Polynomial(VAR, Polynomial.toTermList(newTerm2));
                    resultTF.setText(a.add(b).toString());
                    printButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            try {
                                printTransaction(a, b, "Sum", a.add(b).toString(), false);
                            } catch (IOException exc) {
                            }
                        }
                    });
                }catch (IllegalArgumentException x){
                    resultTF.setText("Invalid Input!");
                    Toolkit.getDefaultToolkit().beep();
                }
                break;
            case "SUBTRACT":
                try {
                    newTerm1 = Polynomial.toStringTermArray(poly1TF.getText());
                    a = new Polynomial(VAR, Polynomial.toTermList(newTerm1));
                    newTerm2 = Polynomial.toStringTermArray(poly2TF.getText());
                    b = new Polynomial(VAR, Polynomial.toTermList(newTerm2));
                    resultTF.setText(a.subtract(b).toString());
                    printButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            try {
                                printTransaction(a, b, "Difference", a.subtract(b).toString(), false);
                            } catch (IOException exc) {
                            }
                        }
                    });
                }catch (IllegalArgumentException x){
                    resultTF.setText("Invalid Input!");
                    Toolkit.getDefaultToolkit().beep();
                }
                break;
            case "MULTIPLY":
                try {
                    newTerm1 = Polynomial.toStringTermArray(poly1TF.getText());
                    a = new Polynomial(VAR, Polynomial.toTermList(newTerm1));
                    newTerm2 = Polynomial.toStringTermArray(poly2TF.getText());
                    b = new Polynomial(VAR, Polynomial.toTermList(newTerm2));
                    resultTF.setText(a.multiply(b).toString());
                    printButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            try {
                                printTransaction(a, b, "Product", a.multiply(b).toString(), false);
                            } catch (IOException exc) {
                            }
                        }
                    });
                }catch (IllegalArgumentException x){
                    resultTF.setText("Invalid Input!");
                    Toolkit.getDefaultToolkit().beep();
                }
                break;
            case "DIVIDE":
                newTerm1 = Polynomial.toStringTermArray(poly1TF.getText());
                a = new Polynomial(VAR,Polynomial.toTermList(newTerm1));
                newTerm2 = Polynomial.toStringTermArray(poly2TF.getText());
                b = new Polynomial(VAR,Polynomial.toTermList(newTerm2));
                /*
                //resultTF.setText(a.divide(b).toString());
                resultTF.setText(a.divide(b).toString());
                printButton.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try{
                            printTransaction(a,b,"Product",a.multiply(b).toString(),false);
                        }catch(IOException exc){
                        }
                    }
                });
                */
                break;
            case "CLEAR":
                poly1TF.setText("");
                poly2TF.setText("");
                resultTF.setText("");
                varTF.setText("");
                Toolkit.getDefaultToolkit().beep();
                break;
        }
    }

    public void evaluateTerm(int evalPoly){
        double eval = 0;
        String newTerm[];
        Polynomial a;
        try {
            if (evalPoly == 1 ) {
                newTerm = Polynomial.toStringTermArray(poly1TF.getText());
            }else
                newTerm = Polynomial.toStringTermArray(poly2TF.getText());
            a = new Polynomial(VAR, Polynomial.toTermList(newTerm));
            eval = Double.parseDouble(varTF.getText());
            resultTF.setText(a.evaluate(eval) + "");
            printButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        printTransaction(a, null, "Evaluated Value", (a.evaluate(Double.parseDouble(varTF.getText())) + ""), true);
                    } catch (IOException exc) {
                    }
                }
            });
        }catch (IllegalArgumentException x){
            resultTF.setText("Invalid Input!");
            Toolkit.getDefaultToolkit().beep();
        }
    }

    public void printTransaction(Polynomial a, Polynomial b, String res,String answer, boolean evaluate) throws IOException{
        Date date = new Date();
        File file = new File("TRANSACTIONS.txt");
        if(!file.exists()){
            file.createNewFile();
        }
        FileWriter fileWriter = new FileWriter(file,true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        PrintWriter print = new PrintWriter(bufferedWriter);
        print.println("");
        print.println("Date of Transaction: " + date);
        if (!evaluate) {
            print.println("New User Input 1: " + a);
            print.println("New User Input 2: " + b);
            print.println("" + res + ":" + answer);
        }else{
            print.println("New User Input: " + a);
            print.println("x = " + Double.parseDouble(varTF.getText()));
            print.println("" + res + ":" + answer);
        }
        print.close();
        resultTF.setText("Execution has been saved to TRANSACTION.txt file");
    }
}
